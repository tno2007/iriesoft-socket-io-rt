// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var ltv = require('log-tv');
var u = require('util');
const cheerio = require('cheerio');
var fs = require('fs');

var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
const parse5 = require('parse5');
var xmlserializer = require('xmlserializer');
//var htmlparser = require("htmlparser2");
//const xml = require("xml-parse");
var tidy = require('htmltidy2').tidy;

var libxmljs = require("libxmljs");

ltv.setup(app, "/log", io);

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';


server.listen(port, ip);
console.log('Iriesoft Server is running since ' + ' on http://%s:%s', ip, port);

// Routing
app.get('/', function (req, res) {
	ltv.log('visiting /');
	res.send('Hi there ' + req.headers['user-agent']);
});

app.get('/teams', function (req, res) {
	ltv.log('visiting /teams');
	res.send('/team endpoint');
});

function y(xml, xquery){
	var doc = new dom().parseFromString(xml);
	return xpath.select(xquery, doc);
}

function yv(xml, xquery){
	var selection = y(xml, xquery);
	if (selection.length > 0) {
		return selection[0].textContent;
	} else {
		return "";
	}
}

app.get('/teams/:id', function (req, res) {
	var id = req.params.id
	ltv.log(u.format('visiting /teams/%s', id));

	var players = [];

	// read file
	var html = "";
	try {  
		var html = fs.readFileSync(u.format('./templates/%s.html', id), 'utf8');
	} catch(e) {
		console.log('Error:', e.stack);
	}


	//var document = parse5.parse(html);
	//var serializedDocument = parse5.serialize(document);		
	//var doc = new dom().parseFromString(serializedDocument);
	var nodes = y(html, "//div[@id='yw1']/table/tbody/tr");

	for (var i = 0; i < nodes.length; i++){

		var name = yv(nodes[i].toString(), '//span[@class="hide-for-small"]');
		var code = yv(nodes[i].toString(), '//span[@class="hide-for-small"]/a/@id');
        var position = yv(nodes[i].toString(), '//tr[2]/td')
		var dob = yv(nodes[i].toString(), '//td[text()][2]');
        var age = yv(nodes[i].toString(), '//td[text()][2]');
		var club_code = yv(nodes[i].toString(), "//a[@class='vereinprofil_tooltip']/@id");
        var height = yv(nodes[i].toString(), '//td[text()][3]');
        var foot = yv(nodes[i].toString(), '//td[text()][4]');
        var caps = yv(nodes[i].toString(), '//td[text()][5]');
        var goals = yv(nodes[i].toString(), '//td[text()][6]');
		var market_value = yv(nodes[i].toString(), '//td[text()][8]');
		
		var player = {
            "code" : code,
            "name" : name,
            "position" : position,
            "dob" : dob,
            "age" : age,
            "club_code" : club_code,
            "height" : height,
            "foot" : foot,
            "caps" : caps,
            "goals" : goals,
            "market_value" : market_value
		}

		players.push(player);

	}

	// **************************************
	// xpath part
	// **************************************
	//var xmlDoc = libxmljs.parseHtmlString(html);
	//var result = xmlDoc.find("//div[@id='yw1']/table/tbody/tr");



	/*
	result.forEach(function(value, index){
		console.log("Current index is " + index);
		var name = value.find('//span[@class="hide-for-small"]')[index].text();
        var code = value.find('//span[@class="hide-for-small"]/a/@id')[index].value();
        var position = value.find('//table[@class="inline-table"]/tr[2]/td')[index].text();
        var dob = value.find('td[3]')[index];
        var age = value.find('//tr[1]/td[3]')[index]
        var club_code = value.find('//tr[1]/td[4]/a/@id')[index]
        var height = value.find('//tr[1]/td[5]')[index]
        var foot = value.find('//tr[1]/td[6]')[index]
        var caps = value.find('//tr[1]/td[7]')[index]
        var goals = value.find('//tr[1]/td[8]')[index]
        var market_value = value.find('//tr[1]/td[10]')[index]
		
		var player = {
            "code" : code,
            "name" : name,
            "position" : position,
            "dob" : dob,
            "age" : age,
            "club_code" : club_code,
            "height" : height,
            "foot" : foot,
            "caps" : caps,
            "goals" : goals,
            "market_value" : market_value
		}

		players.push(player);
	});
*/
	/*
	for (var i = 0; i < result.length; i++){

		

		var name = result[i].get('span[@class="hide-for-small"]').text();
        var code = result[i].get('span[@class="hide-for-small"]/a/@id').value();
        var position = result[i].get('table[@class="inline-table"]/tr[2]/td').text();
		var dob = result[i].get('td[3]').text()

		var player = {
            "code" : code,
            "name" : name,
            "position" : position,
            "dob" : dob
		}

		players.push(player);
	}
	*/

	res.send(players);

});

/*================ FUNCTIONS START ===========================*/


/*================ FUNCTIONS END =============================*/

var numUsers = 0;
var ListOfClients = [];
var bus_id_list = [];

io.on('connection', function (socket) {
	
	socket.on('add user', function (username) {
		//we have to make sure the this list always contains only one reference to the user
		var client = {	
			username: username,
			socket: socket,
			is_bus: false,
			track_bus: null
		};
		ListOfClients.push(client);
		
		var m = "Welcome to Bus Tracker " + username + ". Tap on Find A Bus to request a list of all available busses. If the request is successfull tap on a bus to request its location.";
	
		// Send to current client
		socket.emit('message', m);
	});

	socket.on('disconnect', function () {
		if (addedUser) {
			--numUsers;

			removeClient(socket);
		}
	});
});